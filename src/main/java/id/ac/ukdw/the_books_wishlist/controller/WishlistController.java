package id.ac.ukdw.the_books_wishlist.controller;


import id.ac.ukdw.the_books_wishlist.dto.request.WishlistRequest;
import id.ac.ukdw.the_books_wishlist.dto.response.ResponseWrapper;
import id.ac.ukdw.the_books_wishlist.exception.BadRequestException;
import id.ac.ukdw.the_books_wishlist.service.WishlistService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static id.ac.ukdw.the_books_wishlist.config.SwaggerConfig.*;

@Controller
@RequestMapping("/wishlist")
@Api(tags = wishlistServiceTag)
@RequiredArgsConstructor
public class WishlistController{

    private final WishlistService wishlistService;

    @GetMapping(value = "/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mendapatkan semua wishlist user")
    public ResponseEntity<ResponseWrapper> getWishlist(@RequestParam("email") String email){
        return ResponseEntity.ok(new ResponseWrapper(wishlistService.getAllWishlist(email)));
    }

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menyimpan data wishlist")
    public ResponseEntity<ResponseWrapper> saveWishlist(@RequestBody WishlistRequest request){
        return ResponseEntity.ok(
            new ResponseWrapper(
                wishlistService.saveWishlist(
                    request.getEmail(),
                    request.getIsbn()
                )
            )
        );
    }

    @DeleteMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menghapus data wishlist")
    public ResponseEntity<ResponseWrapper> deleteWishlist(@PathVariable("id") String id){
        return ResponseEntity.ok(
            new ResponseWrapper(
               wishlistService.deleteWishlist(id)
            )
        );
    }

    @GetMapping(value = "/status/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mendapatkan detail wishlist user")
    public ResponseEntity<ResponseWrapper> getDataWishlist(
            @RequestParam("isbn")String isbn,
            @RequestParam("email") String email){
        return ResponseEntity.ok(new ResponseWrapper(
                wishlistService.getWishlist(
                        isbn,
                        email
                )));
    }
}