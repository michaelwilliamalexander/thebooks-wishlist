package id.ac.ukdw.the_books_wishlist.dto;

import lombok.Data;

@Data
public class WishlistDTO {
    
    private int idWishlist;
    
    private String isbn;
    
    private String email;
}
