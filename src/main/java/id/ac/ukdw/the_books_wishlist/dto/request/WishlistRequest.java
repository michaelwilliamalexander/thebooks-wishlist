package id.ac.ukdw.the_books_wishlist.dto.request;

import lombok.Data;

@Data
public class WishlistRequest {
    
    private String email;
    
    private String isbn;

    public WishlistRequest(){}

    public WishlistRequest(String isbn, String email){
        this.isbn = isbn;
        this.email = email;
    }
}
