package id.ac.ukdw.the_books_wishlist.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "wishlist")
public class Wishlist {

    @Id
    @Column(name = "id_wishlist",nullable = false)
    private int idWishlist;

    @Column(name="isbn",nullable = false)
    private String isbn;

    @Column(name = "email_user",nullable = false)
    private String email;

    public Wishlist(){};
    
    public Wishlist(String isbn, String email){
        this.isbn = isbn;
        this.email = email;
    }

}
