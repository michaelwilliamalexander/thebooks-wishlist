package id.ac.ukdw.the_books_wishlist.repository;

import id.ac.ukdw.the_books_wishlist.model.Wishlist;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import id.ac.ukdw.the_books_wishlist.repository.dao.WishlistDao;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.transaction.Transactional;

@Transactional
@Repository
@RequiredArgsConstructor
public class WishlistRepository implements WishlistDao {

    private final EntityManager entityManager;

    private final Logger log = Logger.getLogger(WishlistRepository.class.getSimpleName());

    @Override
    public Optional<Wishlist> findById(String id) {
        String hql = "select wsh from Wishlist wsh where wsh.idWishlist = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        List<Wishlist> wishlist = query.getResultList();
        Wishlist wishlistObject = null;
        for(Wishlist wish:wishlist){
            wishlistObject = wish;
        }
        return Optional.ofNullable(wishlistObject);
    }

    @Override
    public boolean save(Wishlist wishlist) {
        String hql = "insert into wishlist(isbn,email_user) values(?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1, wishlist.getIsbn());
        query.setParameter(2, wishlist.getEmail());
        query.executeUpdate();
        return true;
    }

    @Override
    public boolean delete(String s) {
        String hql = "delete from Wishlist wsh where wsh.idWishlist = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", s);
        query.executeUpdate();
        return true;
    }

    @Override
    public Optional<List<Wishlist>> findByUserEmail(String email) {
        String hql = "select whs from Wishlist whs where whs.email = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email",email);
        List<Wishlist> dataResult = query.getResultList();
        return Optional.ofNullable(dataResult);
    }

    @Override
    public Optional<Wishlist> findByIsbnInEmail(String isbn, String email) {
        String hql = "select wsh from Wishlist wsh where wsh.isbn = :isbn and wsh.email = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn",isbn);
        query.setParameter("email", email);
        List<Wishlist> wishlist = query.getResultList();
        Wishlist wishlistObject = null;
        for(Wishlist wish:wishlist){
            wishlistObject = wish;
        }
        return Optional.ofNullable(wishlistObject);
    }
}