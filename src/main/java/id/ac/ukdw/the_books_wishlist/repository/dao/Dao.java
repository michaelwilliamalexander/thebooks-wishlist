package id.ac.ukdw.the_books_wishlist.repository.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<T,ID>
{
    Optional<T> findById(ID id);

    boolean save(T t);

    boolean delete(ID id);
    

}