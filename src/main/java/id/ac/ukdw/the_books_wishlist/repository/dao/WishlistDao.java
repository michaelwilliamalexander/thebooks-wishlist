package id.ac.ukdw.the_books_wishlist.repository.dao;

import id.ac.ukdw.the_books_wishlist.model.Wishlist;

import java.util.List;
import java.util.Optional;

public interface WishlistDao extends Dao<Wishlist, String> {

    Optional<List<Wishlist>> findByUserEmail(String email);
    
    Optional<Wishlist> findByIsbnInEmail(String isbn, String email);
}