package id.ac.ukdw.the_books_wishlist.service;

import id.ac.ukdw.the_books_wishlist.dto.WishlistDTO;
import id.ac.ukdw.the_books_wishlist.exception.BadRequestException;
import id.ac.ukdw.the_books_wishlist.exception.NotFoundException;
import id.ac.ukdw.the_books_wishlist.model.Wishlist;
import id.ac.ukdw.the_books_wishlist.repository.dao.WishlistDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class WishlistService {

    private final WishlistDao wishlistRepository;

    private final ModelMapper mapper;

    public List<WishlistDTO> getAllWishlist(String emailUser){
        Optional<List<Wishlist>> wishlist = wishlistRepository.findByUserEmail(emailUser);
        if (wishlist.isPresent()){
            List<WishlistDTO> result = new ArrayList<>();
            for (Wishlist data: wishlist.get()){
                WishlistDTO dto = mapper.map(data,WishlistDTO.class);
                result.add(dto);
            }
            return result;
        }
        throw new NotFoundException();
    }

    public boolean getWishlist(String isbn, String email){
        Optional<Wishlist> findData = wishlistRepository.findByIsbnInEmail(isbn,email);
        if (findData.isPresent()){
            return true;
        }
        return false;
    }

    public String saveWishlist(String email, String isbn){
        validate(!isbn.isEmpty(), new BadRequestException());
        validate(!email.isEmpty(),new BadRequestException());
        validate(isbn !=null, new BadRequestException());
        validate(email !=null, new BadRequestException());

        Optional<Wishlist> findData = wishlistRepository.findByIsbnInEmail(isbn, email);
        if (findData.isEmpty()){
            if (wishlistRepository.save(new Wishlist(isbn,email))) {
                return "Wishlist Ditambahkan";
            }
        }
        throw new BadRequestException();
    }

    public String deleteWishlist(String id){
        Optional<Wishlist> find = wishlistRepository.findById(id);
        if(find.isPresent()){
           if(wishlistRepository.delete(id)) {
               return "Wishlist Terhapus";
           }
        }
        throw new BadRequestException();
    }
    
}