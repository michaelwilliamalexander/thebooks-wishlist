package id.ac.ukdw.the_books_wishlist;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", features = "src/test/resources/features" )
@SpringBootTest(classes = {TheBooksWishlistApplication.class
        , Runner.class})
public class RunnerTest {
}
