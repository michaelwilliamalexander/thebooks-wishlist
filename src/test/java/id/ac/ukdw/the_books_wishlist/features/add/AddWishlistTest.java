package id.ac.ukdw.the_books_wishlist.features.add;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ukdw.the_books_wishlist.config.ObjectMapping;
import id.ac.ukdw.the_books_wishlist.controller.WishlistController;
import id.ac.ukdw.the_books_wishlist.dto.request.WishlistRequest;
import id.ac.ukdw.the_books_wishlist.model.Wishlist;
import id.ac.ukdw.the_books_wishlist.service.WishlistService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddWishlistTest {

    @InjectMocks
    private WishlistController controller;

    @Mock
    private WishlistService service;

    private MockMvc mockMvc;

    private List<Wishlist> wishlists;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        wishlists = new ArrayList<>();
    }

    @Given("^Terdapat data-data Wishlist$")
    public void data(List<Wishlist> dataWishlist) {
        wishlists = dataWishlist;
    }

    @When("^user dengan email (.*) menambahkan wishlist buku dengan isbn (.*)$")
    public void input(final String email, final String isbn) {
        when(service.saveWishlist(email,isbn)).thenReturn("Wishlist Ditambahkan");
    }
    @Then("^user menerima (.*) dari data isbn (.*) dan (.*)$")
    public void output(final String pesan, final String isbn, final String email) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/wishlist/")
                .content(ObjectMapping.asJsonString(new WishlistRequest(isbn,email)))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(pesan)))
                .andDo(print());

        verify(service).saveWishlist(email,isbn);
    }
}
