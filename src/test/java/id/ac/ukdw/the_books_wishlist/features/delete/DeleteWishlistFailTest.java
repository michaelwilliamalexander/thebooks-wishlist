package id.ac.ukdw.the_books_wishlist.features.delete;

import id.ac.ukdw.the_books_wishlist.controller.WishlistController;
import id.ac.ukdw.the_books_wishlist.exception.BadRequestException;
import id.ac.ukdw.the_books_wishlist.exception.NotFoundException;
import id.ac.ukdw.the_books_wishlist.model.Wishlist;
import id.ac.ukdw.the_books_wishlist.service.WishlistService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.NotActiveException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DeleteWishlistFailTest {

    @InjectMocks
    private WishlistController controller;

    @Mock
    private WishlistService service;

    private MockMvc mockMvc;

    private List<Wishlist> wishlists;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        wishlists = new ArrayList<>();
    }

    @Given("^List data-data wishlist$")
    public void terlist_wishlist(List<Wishlist> data) {
       wishlists = data;
    }

    @When("^user menghapus data wishlist dengan data id (.*)$")
    public void input(final String id) {
        when(service.deleteWishlist(id)).thenThrow(new BadRequestException());
    }

    @Then("^user mendapatkan Bad Request Exception dari data request (.*)$")
    public void output(final String id) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/wishlist/{id}",id))
                .andExpect(status().isBadRequest())
                .andDo(print());

        verify(service).deleteWishlist(id);
    }
}
