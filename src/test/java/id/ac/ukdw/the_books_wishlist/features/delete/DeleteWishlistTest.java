package id.ac.ukdw.the_books_wishlist.features.delete;

import id.ac.ukdw.the_books_wishlist.controller.WishlistController;
import id.ac.ukdw.the_books_wishlist.model.Wishlist;
import id.ac.ukdw.the_books_wishlist.service.WishlistService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class DeleteWishlistTest {

    @InjectMocks
    private WishlistController controller;

    @Mock
    private WishlistService service;

    private MockMvc mockMvc;

    private List<Wishlist> wishlists;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        wishlists = new ArrayList<>();
    }

    @Given("^Terlist data wishlist$")
    public void terlist_data_wishlist(List<Wishlist> data) {
        wishlists = data;
    }

    @When("^user menghapus data wishlist dengan id (.*)$")
    public void input(final String id) {
        for (Wishlist wishlist : wishlists){
            if(id.equals(String.valueOf(wishlist.getIdWishlist())))
                when(service.deleteWishlist(id)).thenReturn("Wishlist Terhapus");
            }
    }

    @Then("^user mendapatkan pesan (.*) dari request data (.*)$")
    public void output(final String pesan, final String id) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/wishlist/{id}",id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(pesan)))
                .andDo(print());

        verify(service).deleteWishlist(id);
    }
}
