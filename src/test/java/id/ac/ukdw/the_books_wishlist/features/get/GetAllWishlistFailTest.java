package id.ac.ukdw.the_books_wishlist.features.get;

import id.ac.ukdw.the_books_wishlist.config.ObjectMapping;
import id.ac.ukdw.the_books_wishlist.controller.WishlistController;
import id.ac.ukdw.the_books_wishlist.exception.NotFoundException;
import id.ac.ukdw.the_books_wishlist.service.WishlistService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllWishlistFailTest {

    @InjectMocks
    private WishlistController controller;

    @Mock
    private WishlistService service;

    private MockMvc mockMvc;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @When("^user beremail (.*) ingin mendapatkan semua data wishlist$")
    public void input(final String email) {
        when(service.getAllWishlist(email)).thenThrow(new NotFoundException());;
    }

    @Then("^user mendapatkan pesan Not Found data wishlist berdasarkan (.*)$")
    public void output(final String email) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/wishlist/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("email",email))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(service).getAllWishlist(email);
    }
}
