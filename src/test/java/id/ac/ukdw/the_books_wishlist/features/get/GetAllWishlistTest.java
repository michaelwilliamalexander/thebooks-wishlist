package id.ac.ukdw.the_books_wishlist.features.get;

import id.ac.ukdw.the_books_wishlist.config.ObjectMapping;
import id.ac.ukdw.the_books_wishlist.controller.WishlistController;
import id.ac.ukdw.the_books_wishlist.dto.WishlistDTO;
import id.ac.ukdw.the_books_wishlist.service.WishlistService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllWishlistTest {

    @InjectMocks
    private WishlistController controller;

    @Mock
    private WishlistService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<WishlistDTO> dataResult;

    private List<WishlistDTO> wishlists;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        wishlists = new ArrayList<>();
        dataResult = new ArrayList<>();
    }

    @Given("^Terdapat list wishlist$")
    public void data(List<WishlistDTO> data) {
        wishlists = data;
    }

    @When("^user dengan email (.*) ingin mendapatkan semua data wishlist$")
    public void input(final String email) {
        for (WishlistDTO data: wishlists){
            if (data.getEmail().equals(email)){
                dataResult.add(mapper.map(data,WishlistDTO.class));
            }
        }
        when(service.getAllWishlist(email)).thenReturn(dataResult);
    }
    @Then("^user mendapatkan semua data wishlist berdasarkan (.*)$")
    public void output(final String email) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/wishlist/")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .param("email",email))
                .andExpect(jsonPath("$.data",hasSize(dataResult.size())))
                .andExpect(status().isOk())
                .andDo(print());

        verify(service).getAllWishlist(email);
    }
}
