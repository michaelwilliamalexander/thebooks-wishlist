package id.ac.ukdw.the_books_wishlist.features.get;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.ac.ukdw.the_books_wishlist.config.ObjectMapping;
import id.ac.ukdw.the_books_wishlist.controller.WishlistController;
import id.ac.ukdw.the_books_wishlist.dto.WishlistDTO;
import id.ac.ukdw.the_books_wishlist.dto.request.WishlistRequest;
import id.ac.ukdw.the_books_wishlist.model.Wishlist;
import id.ac.ukdw.the_books_wishlist.service.WishlistService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetStatusWishlistTest {

    @InjectMocks
    private WishlistController controller;

    @Mock
    private WishlistService service;

    private MockMvc mockMvc;

    private List<WishlistDTO> wishlists;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        wishlists = new ArrayList<>();
    }

    @Given("^List data wishlist$")
    public void list_data_wishlist(List<WishlistDTO> data) {
       wishlists = data;
    }

    @When("^User dengan email (.*) ingin mendapatkan data wishlist dengan isbn (.*)$")
    public void input(final String email, final String isbn) {
        for (WishlistDTO item:wishlists){
            if (item.getEmail().equals(email)&& item.getIsbn().equals(isbn)){
                when(service.getWishlist(isbn,email)).thenReturn(true);
                break;
            }else{
                when(service.getWishlist(isbn,email)).thenReturn(false);
            }
        }

    }

    @Then("^User mendapatkan status (.*) dengan request (.*) dan (.*)$")
    public void output(final boolean status, final String email, final String isbn) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/wishlist/status/")
                .param("email",email)
                .param("isbn",isbn))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data",is(status)))
                .andDo(print());

        verify(service).getWishlist(isbn,email);
    }
}
