Feature: Menambahkan Data Wishlist

  Scenario Outline: Menambahkan Wishlist dan Berhasil
    Given Terdapat data-data Wishlist
    |idWishlist |isbn           |email                            |
    |1          |9781974710041  |michael.william@ti.ukdw.ac.id    |
    |2          |9780804171588  |nathaniel.alvin@ti.ukdw.ac.id    |
    When user dengan email <email> menambahkan wishlist buku dengan isbn <isbn>
    Then user menerima <pesan> dari data isbn <isbn> dan <email>
    Examples:
    |email                            |isbn         |pesan                |
    |michael.william@ti.ukdw.ac.id    |9780804171588|Wishlist Ditambahkan |
    |nathaniel.alvin@ti.ukdw.ac.id    |9781974710041|Wishlist Ditambahkan |


  Scenario Outline: Menambahkan Wishlist dan Gagal
    Given Terdapat data Wishlist
      |idWishlist |isbn           |email                            |
      |1          |9781974710041  |michael.william@ti.ukdw.ac.id    |
      |2          |9780804171588  |michael.william@ti.ukdw.ac.id    |
      |3          |9780804171588  |nathaniel.alvin@ti.ukdw.ac.id    |
    When user dengan email <email> menambahkan wishlist isbn buku <isbn>
    Then user mendapatkan Bad Request terhadap request data <isbn> dan <email>
    Examples:
      |email                            |isbn         |
      |michael.william@ti.ukdw.ac.id    |9780804171588|
      |nathaniel.alvin@ti.ukdw.ac.id    |9780804171588|
      |nathaniel.alvin@ti.ukdw.ac.id    |             |
      |                                 |9780804171588|
      |                                 |             |