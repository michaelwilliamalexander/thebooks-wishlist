Feature: Menghapus Data Wishlist

  Scenario Outline: Menghapus Wishlist dan Berhasil
    Given Terlist data wishlist
    |idWishlist   |isbn           |userEmail                        |
    |1            |9781974710041  |michael.william@ti.ukdw.ac.id    |
    |2            |9780804171588  |michael.william@ti.ukdw.ac.id    |
    |3            |9781974710041  |nathaniel.alvin@ti.ukdw.ac.id    |
    |4            |9780804171588  |nathaniel.alvin@ti.ukdw.ac.id    |
    When user menghapus data wishlist dengan id <id>
    Then user mendapatkan pesan <pesan> dari request data <id>
    Examples:
      |id |pesan                |
      |1  |Wishlist Terhapus    |
      |2  |Wishlist Terhapus    |

  Scenario Outline: Menghapus Wishlist dan Gagal
    Given List data-data wishlist
      |ididWishlist   |isbn           |userEmail                        |
      |1              |9781974710041  |michael.william@ti.ukdw.ac.id    |
      |4              |9780804171588  |nathaniel.alvin@ti.ukdw.ac.id    |
    When user menghapus data wishlist dengan data id <id>
    Then user mendapatkan Bad Request Exception dari data request <id>
    Examples:
      |id   |
      |2    |
      |3    |

