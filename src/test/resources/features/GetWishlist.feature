Feature: Get Data Wishlist

  Scenario Outline: Mendapatkan status data Wishlist
    Given List data wishlist
      |idWishlist   |isbn           |email                            |
      |1            |9781974710041  |michael.william@ti.ukdw.ac.id    |
      |2            |9780804171588  |michael.william@ti.ukdw.ac.id    |
      |3            |9781974710041  |nathaniel.alvin@ti.ukdw.ac.id    |
    When User dengan email <email> ingin mendapatkan data wishlist dengan isbn <isbn>
    Then User mendapatkan status <status> dengan request <email> dan <isbn>
    Examples:
      |isbn           |email                            |status     |
      |9780804171588  |michael.william@ti.ukdw.ac.id    |true       |
      |9781974710041  |nathaniel.alvin@ti.ukdw.ac.id    |true       |
      |9780804171588  |nathaniel.alvin@ti.ukdw.ac.id    |false      |

  Scenario Outline: Mendapatkan semua data wishlist dan Ditemukan
    Given Terdapat list wishlist
      |idWishlist   |isbn           |email                            |
      |1            |9781974710041  |michael.william@ti.ukdw.ac.id    |
      |2            |9780804171588  |michael.william@ti.ukdw.ac.id    |
      |3            |9781974710041  |nathaniel.alvin@ti.ukdw.ac.id    |
      |4            |9780804171588  |nathaniel.alvin@ti.ukdw.ac.id    |
    When user dengan email <email> ingin mendapatkan semua data wishlist
    Then user mendapatkan semua data wishlist berdasarkan <email>
    Examples:
      |email                            |
      |michael.william@ti.ukdw.ac.id    |
      |nathaniel.alvin@ti.ukdw.ac.id    |

  Scenario Outline: Mendapatkan semua data wishlist dan Tidak Ditemukan
    When user beremail <email> ingin mendapatkan semua data wishlist
    Then user mendapatkan pesan Not Found data wishlist berdasarkan <email>
    Examples:
      |email                            |
      |michael.william@ti.ukdw.ac.id    |
      |nathaniel.alvin@ti.ukdw.ac.id    |
